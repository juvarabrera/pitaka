# pitaka

![pitaka-animation](/uploads/9cee9d63170d4e552ed65e95fb28d8e2/pitaka-animation.gif)

Pitaka, from a Filipino word meaning 'wallet', is an open-source budget tracking web application. This project is for fun only. I am trying to develop an application from technologies I have not used before from my professional work. 

`main` branch for now will remain empty except this README.md. See `v0.0.1` branch for the current development. You can also check out the [Gitlab Issue Board](https://gitlab.com/juvarabrera/pitaka/-/boards) for the progress of the application.
